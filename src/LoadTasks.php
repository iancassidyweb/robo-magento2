<?php
namespace Cassidy\Robo\Task\Magento2;

trait loadTasks
{
    protected function taskMagento2()
    {
        return $this->task(TaskValet::class);
    }
}
